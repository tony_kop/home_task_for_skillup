function CreateInput (sel) {
  let div = document.querySelector(sel);
  let input = document.createElement('input');
  input.type = "text";
  div.appendChild(input);

  let btn = {
    add: document.createElement('button'),
    remove: document.createElement('button'),
    removeAll: document.createElement('button'),
  }; 

  btn.add.innerText = 'Add name';
  btn.remove.innerText = 'Remove name';
  btn.removeAll.innerText = 'Remove all name';

  for (b in btn) {
    btn[b].type = 'button';
    div.appendChild(btn[b]);
  };

  let divNames = document.createElement('div');
  div.appendChild(divNames);
  let ul = document.createElement('ul');
  divNames.appendChild(ul);
  

  let names = [];

  function showNames()  {
    divNames.innerHTML = '';
    names.forEach(name => divNames.innerHTML += `<li>${name}</li>`);
  };

  function addName() {
    let val = input.value.trim();
    if(!~names.indexOf(val) && val) {
      names.push(val);
      showNames();
    }
    else alert(val + ' - такое имя уже есть в списке или Вы ничего не передали');
  };
  btn.add.onclick = addName;

  btn.remove.onclick = function () {
    if(names.length < 1) return alert('У нас всё уже удалено'); 
    alert(names.pop() + ' удален!');
    showNames();
  };

  function removeAllNames () {
    names = [];
    divNames.innerHTML = '';
    input.value = '';
    alert('Всё нафиг удалено');
  };

  btn.removeAll.onclick = removeAllNames;
};

let inputNames = new CreateInput('#input');