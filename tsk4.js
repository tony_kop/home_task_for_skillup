function CreateMoveBox(sel, div) {
  let boxArea = document.querySelector(sel);
  div = document.querySelector(div);

  let box = document.createElement('div');
  box.className = 'move-box';
  boxArea.appendChild(box);

  let buttons = {
    left: document.createElement('button'),
    right: document.createElement('button'),
    down: document.createElement('button'),
    up: document.createElement('button'),
    center: document.createElement('button')
  };

  for (btn in buttons) {
    buttons[btn].type = 'button';
    buttons[btn].innerText = btn;
    div.insertBefore(buttons[btn], boxArea); 
  }

  let rightX, topY;

  function resetToCenter(){
    rightX = 50;
    topY = 50;
    box.style.right = rightX + "%";
    box.style.top = topY + "%";
  };
  resetToCenter();

  function moveRight() {
    rightX -= 10;
    if(rightX < 0) rightX = 96;
    box.style.right = rightX + "%";
    if(rightX >= 96) rightX = 90;
  };

  function moveLeft() {
    if (!isNaN(rightX)) rightX += 10;
    else  rightX = 0;
    if(rightX > 96) rightX = 96;
    box.style.right = rightX + "%";
    if(rightX === 96) rightX = NaN;
  };

  function moveUp() {
    topY -= 10;
    if(topY < 0) topY = 94;
    box.style.top = topY + "%";
    if(topY >= 94) topY = 90;
  };

  function moveDown() {
    if(!isNaN(topY)) topY += 10; 
    else topY = 0;
    if(topY > 94) topY = 94;
    box.style.top = topY + "%";
    if(topY === 94) topY = NaN;
  };

  function move(side) {
    switch(side){
      case 'right':   return moveRight;
      case 'left':    return moveLeft;
      case 'up':      return moveUp;
      case 'down':    return moveDown;
      case 'center':  return resetToCenter;
    }
  };

  buttons.left.onclick     = move('left');
  buttons.right.onclick    = move('right');
  buttons.up.onclick      = move('up');
  buttons.down.onclick   = move('down');
  buttons.center.onclick   = move('center');
 
};

let moveBox = new CreateMoveBox('.box-area', '.tsk4');