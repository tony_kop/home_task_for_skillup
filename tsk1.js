let arr = ['vasa', 'pitya', 'anya', 'vera', 'alex', 'tony', 'nick', 'shukhrat'];
let names = document.querySelector('#names');
let ul = document.createElement('ul');

function createList(arr){
    if (!Array.isArray(arr)) { 
        console.log ('передан не массив'); return null; }

    arr.forEach(name => ul.innerHTML += `<li class='name'>${name}</li>`);
    names.appendChild(ul);
};
createList(arr);

function onclickForLi(){
    let liNames = Array.from(document.querySelectorAll('.name'));
    liNames.forEach( name => name.onclick = function()  { 
        let txt = (this.innerText === 'shukhrat') ? 'Лучшие кальяны в городе, только у Шухрата где-то на Гагарина...' : this.innerText;
        alert(txt); 
    } );
};

// onclickForLi(); 
// вместо вызова функции onclickForLi() намного меньше кода если вызывать через объект события - event

ul.onclick = (event) => {
    let mess = (event.target.innerText === 'shukhrat') ? 'Лучшие кальяны в городе, только у Шухрата где-то на Гагарина...' : event.target.innerText;
    alert(mess);
};