let namess = ['vasa', 'pitya', 'anya', 'vera', 'alex', 'tony', 'nick', 'shukhrat'];

function UserManager(sel, data) {
  let div = document.querySelector(sel);
  data = (Array.isArray(data)) ? data : [];
  
  let btnList = document.createElement('button');
  btnList.type = 'button';
  btnList.innerText = 'List users';
    
  let btnClear = document.createElement('button');
  btnClear.type = 'button';
  btnClear.innerText = 'Clear';

  let btnFind = document.createElement('button');
  btnFind.type = 'button';
  btnFind.innerText = 'Find user';
  
  let name = document.createElement('input');
  name.type = 'text';
  name.placeholder = 'enter user search...';

  let block = document.createElement('div');

  div.appendChild(btnFind);
  div.appendChild(name);
  div.appendChild(btnList);
  div.appendChild(btnClear);

  function clearBlock() { 
    block.innerHTML = ''; 
    block.remove();
  }
  function findUser() {
    clearBlock();
    let val = name.value.trim();
    if(!val) return alert('Поле пустое. Нужно что-то ввести');
    
    let p = document.createElement('p');
    div.appendChild(block);
    let mess = ''; 
    if(~data.indexOf(val)) 
      mess = `Пользователь: ${val} найден.`;
    else { 
      mess = `Такого пользователя: ${val} нет, но мы его уже добавили.`;
      data.push(val);
    }

    alert(mess);
    p.innerText = mess;
    block.appendChild(p);
  };

  btnFind.onclick = findUser;

  name.onkeypress = (event) => {
    if(event.keyCode === 13)
      findUser();
  };
  
  function listUser() {
    clearBlock();
    div.appendChild(block);
    let ul = document.createElement('ul');
    
    data.forEach(name => ul.innerHTML += `<li>${name}</li>`);
    block.appendChild(ul);
  };

  btnList.onclick = listUser;
  btnClear.onclick = () => { name.value = ''; clearBlock(); };
};

let listUsers = new UserManager('#tsk7', namess);
