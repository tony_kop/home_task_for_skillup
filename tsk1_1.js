let arr = ['vasa', 'pitya', 'anya', 'vera', 'alex', 'tony', 'nick', 'shukhrat'];

function CreateLyst(sel, arr) {
    this.arr = (!Array.isArray(arr)) ? [] : arr;
    this.names = document.querySelector(sel);
    this.ul = document.createElement('ul');
    this.names.appendChild(this.ul);

    this.arr.forEach(name => this.ul.innerHTML += `<li>${name}</li>`);
    this.ul.onclick = (event) => {
        let mess = (event.target.innerText === this.exception) ? this.mess : event.target.innerText;
        alert(mess); 
    }
    this.exceptionOnClick = function(name, mess) {
        this.exception = name;
        this.mess = mess;
    };
};

let lystNames = new CreateLyst('#names', arr);
lystNames.exceptionOnClick('shukhrat', 'Лучшие кальяны в городе, только у Шухрата где-то на Гагарина...');