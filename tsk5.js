let cities = ['Dnipro', 'Kyiv', 'Lviv', 'Odesa', 'Kharkiv', 'Yalta', 'Zaporijjya', 'Donetsk', 'Kherson'];

function CreateTwoBlocksCity(sel, cities) {

  let firstCities = cities.slice(0, 5);
  let secondCities = cities.slice(5);

  let div = document.querySelector(sel);
  let block1 = document.createElement('div');
  let block2 = document.createElement('div');
  div.appendChild(block1);
  div.appendChild(block2);

  block1.style.backgroundColor = 'lightblue';
  block2.style.backgroundColor = 'lightgreen';

  let ul1 = document.createElement('ul');
  ul1.className = 'ul1';
  let ul2 = document.createElement('ul');
  ul2.className = 'ul2';
  block1.appendChild(ul1);
  block2.appendChild(ul2);
  
  function showCities() {
    let cities = firstCities;
    let ul = ul1;
    
    ul.innerHTML = '';
    cities.forEach(city => {
      ul.innerHTML += `<li>${city}&nbsp&nbsp&nbsp - &nbsp&nbsp&nbsp<button type='button' data-action='${city}'>X</button></li>`;
    });

    cities = secondCities;
    ul = ul2;

    ul.innerHTML = '';
    cities.forEach(city => {
      ul.innerHTML += `<li>${city}&nbsp&nbsp&nbsp - &nbsp&nbsp&nbsp<button type='button' data-action='${city}'>X</button></li>`;
    });
  };

  showCities();

  div.onclick = (event) => {
    let action = event.target.getAttribute('data-action');

    if(~firstCities.indexOf(action)) {
      firstCities = firstCities.filter(city => { if (city !== action) return city; } );
      secondCities.push(action);
    }
    else if(~secondCities.indexOf(action)) {
      secondCities = secondCities.filter(city => { if (city !== action) return city; } );
      firstCities.push(action);
    }

    showCities();
  };
  
};

let twoBlocks = new CreateTwoBlocksCity('#tsk5', cities);