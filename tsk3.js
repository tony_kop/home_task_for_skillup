let colors = ['red', 'blue', 'yellow', 'brown', 'cyan', 'magenta', 'khaki', 'green', 'grey', 'pink', 'orange'];

function ChangeColor(sel, arr) {
  arr = (Array.isArray(arr)) ? arr : [];
  let div = document.querySelector(sel);
  let colorBlock = document.createElement('div');
  colorBlock.className = 'color-block';
  div.appendChild(colorBlock);
  
  let btn = {
    next: document.createElement('button'),
    prev: document.createElement('button'),
    rand: document.createElement('button')
  };

  for (b in btn) {
    btn[b].type = 'button';
    btn[b].innerText = b;
    div.insertBefore(btn[b], colorBlock);
  }


  let color = 0;

  function nextColor() {
    color = (++color > arr.length - 1) ? 0 : color;
    colorBlock.style.backgroundColor = arr[color];
  };

  function prevColor() {
    color = (--color < 0) ? arr.length - 1 : color;
    colorBlock.style.backgroundColor = arr[color];
  };

  function randColor() {
    color = Math.floor(Math.random() * arr.length);
    colorBlock.style.backgroundColor = arr[color];
  };

  btn.next.onclick = nextColor;
  btn.prev.onclick = prevColor;
  btn.rand.onclick = randColor;
};

let colorBlock = new ChangeColor('.tsk3', colors);