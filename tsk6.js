function CreateFormsSteps(sel) {
  let div = document.querySelector(sel);
  
  let formData = {};
  let formElements = {
    form: document.querySelector('#form'),
    step1: document.querySelector('#step1'),
    step2: document.querySelector('#step2'),
    step3: document.querySelector('#step3'),
    name: document.querySelector('#name'),
    sirname: document.querySelector('#sirname'),
    age: document.querySelector('#age'),
    email: document.querySelector('#email'),
    telephone: document.querySelector('#number'),
    city: document.querySelector('#city'),
    nextStep1: document.querySelector('#nextStep1'),
    nextStep2: document.querySelector('#nextStep2'),
    nextStep3: document.querySelector('#nextStep3'),
    confirm: document.querySelector('#confirm'),
    btnConfirm: document.querySelector('#btn-confirm'),
    btnEdit: document.querySelector('#btn-edit'),
    mess: document.querySelector('#mess'),
    messEdit: document.querySelector('#mess-edit'),
    ul: document.querySelector('#confirm ul'),
    title: document.querySelector('#title'),
    next1: function(){
      formData.name = formElements.name.value.trim();
      formData.sirname = formElements.sirname.value.trim();
      if(formData.name && formData.sirname)
        formElements.form.appendChild(formElements.step2);
      else alert('Пожалуйста заполните все поля')
    },
    next2: function(){
      formData.age = formElements.age.value.trim();
      formData.email = formElements.email.value.trim();
      if(formData.age && formData.email)
        formElements.form.appendChild(formElements.step3)
      else alert('Пожалуйста заполните все поля')
    },
    next3: function() {
      formData.telephone = formElements.telephone.value.trim();
      formData.city = formElements.city.value.trim();

      if(formData.telephone && formData.city){
        formElements.ul.innerHTML = '';
        for (field in formData){
          let data = ''
          switch(field){
            case 'name':      data = 'Имя'; break;
            case 'sirname':   data = 'Фамилия'; break;
            case 'age':       data = 'Возраст'; break;
            case 'email':     data = 'email'; break;
            case 'telephone': data = 'Номер телефона'; break;
            case 'city':      data = 'Город'; break;
          }
          formElements.ul.innerHTML += `<li><strong>${data}:</strong> ${formData[field]}</li>`;
        }

        formElements.step3.remove();
        formElements.step2.remove();
        formElements.step1.remove();
        formElements.form.appendChild(formElements.confirm);
        formElements.mess.innerText = `Если все введено верно нажмите на кнопку "Подтверждаю":`;
        formElements.title.innerText = `Вы ввели такие данные:`;
        formElements.confirm.insertBefore(formElements.messEdit, formElements.btnConfirm);
        formElements.confirm.appendChild(formElements.btnEdit);
        formElements.btnConfirm.onclick = formElements.accept;
        formElements.btnEdit.onclick = formElements.edit;
      }
    },
    accept: function(){
      formElements.title.innerText = 'Все Ваши данные были успешно сохранены';
      formElements.title.style.backgroundColor = 'green';
      formElements.title.style.color = 'white';
      formElements.ul.style.backgroundColor = 'lightgreen';
      formElements.mess.remove();
      formElements.messEdit.remove();
      formElements.btnConfirm.remove();
      formElements.btnEdit.remove();
      alert(formElements.title.innerText);
    },
    edit: function(){
      formElements.ul.innerHTML = '';

      for (field in formData){
        let data = ''
        let input = '';
        let li = document.createElement('li');
        let strong = document.createElement('strong');
        
        switch(field){
          case 'name':      data = 'Имя: '; 
                            input = formElements.name; break;
          case 'sirname':   data = 'Фамилия: ';
                            input = formElements.sirname; break;
          case 'age':       data = 'Возраст: ';
                            input = formElements.age; break;
          case 'email':     data = 'email: ';
                            input = formElements.email; break;
          case 'telephone': data = 'Номер телефона: ';
                            input = formElements.telephone; break;
          case 'city':      data = 'Город: ';
                            input = formElements.city; break;
        }
        strong.innerText = data;
        li.appendChild(strong);
        li.appendChild(input);
        formElements.ul.appendChild(li);
        formElements.btnEdit.remove();
      }
      formElements.messEdit.remove();
      formElements.title.innerText = `Пожалуйста отредактируйте Ваши данные:`;
      formElements.btnConfirm.innerText = 'Подтверждаю изменения';
      formElements.mess.innerText = `Если все изменения введены верно, нажмите на кнопку "Подтверждаю изменения"`;
      formElements.btnConfirm.onclick = formElements.next3;
    }

  };

  formElements.step2.remove();
  formElements.step3.remove();
  formElements.confirm.remove();

  formElements.nextStep1.onclick = formElements.next1;
  formElements.sirname.onkeypress = (event) => {
    if(event.keyCode === 13)
      formElements.next1();
  };

  formElements.nextStep2.onclick = formElements.next2;
  formElements.email.onkeypress = (event) => {
    if(event.keyCode === 13)
      formElements.next2();
  };

  formElements.nextStep3.onclick = formElements.next3;
  formElements.city.onkeypress = (event) => {
    if(event.keyCode === 13)
      formElements.next3();
  };

  formElements.btnConfirm.onclick = formElements.accept;
};

let formsSteps = new CreateFormsSteps('#tsk6');

